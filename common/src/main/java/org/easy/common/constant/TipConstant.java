package org.easy.common.constant;

/**
 * @ClassName: TipConstant
 * @Description: 提示信息相关常量（注释规则：所用位置+返回结果）
 * @Author zhouzhixin
 * @Date 2021/10/12
 * @Version 1.0
 */
public interface TipConstant {

    /**
     * 通用接口返回信息-登录用户不存在
     */
    String USER_NOT_EXIST = "登录用户不存在";
    /**
     * 注册接口返回提示信息-注册成功
     */
    String REGISTER_SUCCESS = "注册成功";

    /**
     * 注册接口返回提示信息-注册失败
     */
    String REGISTER_FAIL = "注册失败";

    /**
     * 验证用户信息接口返回提示信息-用户名重复
     */
    String CHECK_USER_INFO_USERNAME_REPEAT = "用户名重复，请再换一个吧";

    /**
     * 验证用户信息接口返回提示信息-邮箱重复
     */
    String CHECK_USER_INFO_EMAIL_REPEAT = "邮箱重复，请再换一个吧";

    /**
     * 激活用户接口返回提示信息-激活成功
     */
    String ACTIVATION_SUCCESS = "激活成功";

    /**
     * 激活用户接口返回提示信息-激活失败
     */
    String ACTIVATION_FAIL = "激活失败";

    /**
     * 激活用户接口返回提示信息-验证码过期
     */
    String ACTIVATION_CODE_EXPIRE = "验证码过期，已重新发送邮件，请注意查收";

    /**
     * 激活用户接口返回提示信息-验证码无效
     */
    String ACTIVATION_CODE_INVALID = "验证码无效，请重新输入";

    /**
     * 激活用户接口返回提示信息-未知错误
     */
    String ACTIVATION_UNKNOW_ERROR = "未知错误，请联系系统管理员";

    /**
     * 验证字典是否存在接口-字典名称重复
     */
    String CHECK_DICT_EXIST_DICT_NAME = "字典名称重复";

    /**
     * 验证字典是否存在接口-字典编码重复
     */
    String CHECK_DICT_EXIST_DICT_CODE = "字典编码重复";

    /**
     * 验证数据值重复接口-数据值重复
     */
    String CHECK_DICT_DATA_EXIST_DATA = "相同的数据名称下的数据值重复";

    /**
     * 验证邮件模板数据是否存在接口-邮件主题重复
     */
    String CHECK_MAIL_TEMPLATE_EXIST_SUBJECT = "邮件主题重复";

}
