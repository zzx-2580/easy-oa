package org.easy.common.constant;

/**
 * @ClassName: RedisConstant
 * @Description: Redis相关常量
 * @Author zhouzhixin
 * @Date 2021/10/12
 * @Version 1.0
 */
public interface RedisConstant {

    /**
     * 邮件激活码后缀
     */
    String REGISTER_VALIDATION_CODE_CACHE_SUFFIX = "_EMAIL";

}
