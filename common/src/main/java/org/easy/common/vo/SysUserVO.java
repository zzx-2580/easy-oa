package org.easy.common.vo;

import lombok.*;

/**
 * (SysUser)实体类
 *
 * @author makejava
 * @since 2021-09-26 14:27:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class SysUserVO {
    /**
     * id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 密码
     */
    private String password;
    /**
     * 密码盐
     */
    private String salt;
    /**
     * 性别
     */
    private String sex;
    /**
     * 生日
     */
    private Object birthday;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区县
     */
    private String county;
    /**
     * 街道
     */
    private String street;
    /**
     * 完整地址（选择完省市区后生成）
     */
    private String fullAddress;
    /**
     * 详细地址（用户手动输入）
     */
    private String detailAddress;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 删除标识
     */
    private String deleted;

}
