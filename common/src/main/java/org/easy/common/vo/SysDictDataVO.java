package org.easy.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * (SysDictData)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:57:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class SysDictDataVO {
    /**
     * id
     */
    private String id;
    /**
     * 数据字典id
     */
    private String dictId;
    /**
     * 数据字典名称
     */
    private String dictName;
    /**
     * 数据名称
     */
    private String dataName;
    /**
     * 数据值
     */
    private Integer dataValue;
    /**
     * 数据描述
     */
    private String dataDesc;
    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
