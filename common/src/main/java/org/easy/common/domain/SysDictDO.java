package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.io.Serializable;

/**
 * (SysDict)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:57:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_dict")
public class SysDictDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -94466712551063378L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 字典名称
     */
    private String dictName;
    /**
     * 字典编码
     */
    private String dictCode;
    /**
     * 字典描述
     */
    private String dictDesc;

}
