package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (BzConsumer)实体类
 *
 * @author makejava
 * @since 2021-11-26 16:39:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "bz_consumer")
public class BzConsumerDO implements Serializable {
    private static final long serialVersionUID = 711013523455471835L;
    /**
     * 主键
     */
    private String id;
    /**
     * 序号
     */
    private Integer serialNumber;
    /**
     * 账号ID
     */
    private String accountId;
    /**
     * 来源类型
     */
    private String sourceType;
    /**
     * 部门编码
     */
    private String sysOrgCode;
    /**
     * 所属门店ID
     */
    private String storeId;
    /**
     * 门店名称
     */
    private String storeName;
    /**
     * 护理员
     */
    private String nurseAssistantId;
    /**
     * 护理员名称
     */
    private String nurseAssistantName;
    /**
     * 督导员
     */
    private String supervisorId;
    /**
     * 督导员名称
     */
    private String supervisorName;
    /**
     * 护理产品类型
     */
    private String nurseTypes;
    /**
     * 是否建档
     */
    private String bookBuilding;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别
     */
    private String sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 生日
     */
    private Date birthDate;
    /**
     * 身高
     */
    private Double stature;
    /**
     * 体重
     */
    private Double weight;
    /**
     * 户籍地-省
     */
    private String censusRegisterGroundProvince;
    /**
     * 户籍地-市
     */
    private String censusRegisterGroundCity;
    /**
     * 户籍地
     */
    private String censusRegisterGroundText;
    /**
     * 学历
     */
    private String academicBg;
    /**
     * 民族
     */
    private String nation;
    /**
     * 政治面貌
     */
    private String politicsStatus;
    /**
     * 婚否
     */
    private String maritalStatus;
    /**
     * 原职业
     */
    private String previousOccupation;
    /**
     * 医保卡号
     */
    private String medicalInsuranceCardNumber;
    /**
     * 宗教
     */
    private String religion;
    /**
     * 微信
     */
    private String wechat;
    /**
     * 固定电话
     */
    private String telephone;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区/县
     */
    private String county;
    /**
     * 街道/乡镇
     */
    private String street;
    /**
     * 详细地址/门牌号
     */
    private String detailAddress;
    /**
     * 完整地址
     */
    private String fullAddress;
    /**
     * 状态
     */
    private String status;
    /**
     * 开发人
     */
    private String salesman;
    /**
     * 家庭结构
     */
    private String familyStructure;
    /**
     * 是否失独
     */
    private String losingSingleChild;
    /**
     * 身体状态
     */
    private String physicalCondition;
    /**
     * 居家环境
     */
    private String homeEnvironment;
    /**
     * 社交关系
     */
    private String socialRelation;
    /**
     * 外观状态
     */
    private String apparentCondition;
    /**
     * 沟通能力
     */
    private String communicateAbility;
    /**
     * 判断能力
     */
    private String judgementAbility;
    /**
     * 行走能力
     */
    private String walkAbility;
    /**
     * 自理能力
     */
    private String takeCareOneselfAbility;
    /**
     * 性格特征
     */
    private String traitsCharacter;
    /**
     * 饮食状况
     */
    private String dietetic;
    /**
     * 饮食习惯
     */
    private String dieteticHabit;
    /**
     * 消费特征
     */
    private String consumptionFeature;
    /**
     * 购买能力
     */
    private String purchasingPower;
    /**
     * 兴趣
     */
    private String interests;
    /**
     * 风险评估
     */
    private String riskAssessment;
    /**
     * 既往病史
     */
    private String anamnesis;
    /**
     * 标签
     */
    private String tags;
    /**
     * 特殊项目
     */
    private String specialNurseType;
    /**
     * 常规技能
     */
    private String conventionSkill;
    /**
     * 特殊技能
     */
    private String specialSkill;
    /**
     * 人员需求-性别
     */
    private String requireSex;
    /**
     * 人员需求-年龄
     */
    private String requireAge;
    /**
     * 人员需求-学历
     */
    private String requireAcademicBg;
    /**
     * 人员需求-体型
     */
    private String requireBodily;
    /**
     * 消费需求
     */
    private String requireConsumption;
    /**
     * 特质要求
     */
    private String requirePeculiarity;
    /**
     * 服务等级
     */
    private String serviceGrade;
    /**
     * 客户分类
     */
    private String classify;
    /**
     * 画像分类
     */
    private String portraitClassify;
    /**
     * 客戶死亡
     */
    private String die;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人姓名
     */
    private String createName;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 销售id
     */
    private String salesId;
    /**
     * 客户来源
     */
    private String consumerSource;

}
