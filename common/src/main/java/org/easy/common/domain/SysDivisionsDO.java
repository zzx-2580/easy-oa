package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * (SysDivisions)实体类
 *
 * @author makejava
 * @since 2021-09-26 14:27:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_divisions")
public class SysDivisionsDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -52246692358396487L;
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 父级code
     */
    private String parentCode;
    /**
     * code
     */
    private String value;
    /**
     * name
     */
    private String label;

}
