package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (SysMailRecord)实体类
 *
 * @author makejava
 * @since 2021-10-29 11:16:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_mail_record")
public class SysMailRecordDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 504099899780877579L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 邮件模板id
     */
    private String templateId;
    /**
     * 邮件内容
     */
    private String content;

}
