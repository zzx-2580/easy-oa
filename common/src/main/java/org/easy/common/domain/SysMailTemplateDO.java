package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (SysMailTemplate)实体类
 *
 * @author makejava
 * @since 2021-10-29 11:16:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_mail_template")
public class SysMailTemplateDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 771734141372900832L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String content;
    /**
     * 邮件类型
     */
    private String type;

}
