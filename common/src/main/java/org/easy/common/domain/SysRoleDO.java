package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (SysRole)实体类
 *
 * @author makejava
 * @since 2021-09-26 14:27:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_role")
public class SysRoleDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 171940187083588877L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 角色名
     */
    private String roleName;
    /**
     * 描述
     */
    private String remark;

}
