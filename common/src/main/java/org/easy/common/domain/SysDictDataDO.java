package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (SysDictData)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:57:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_dict_data")
public class SysDictDataDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -61947021725813978L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 数据字典id
     */
    private String dictId;
    /**
     * 数据名称
     */
    private String dataName;
    /**
     * 数据值
     */
    private Integer dataValue;
    /**
     * 数据描述
     */
    private String dataDesc;

}
