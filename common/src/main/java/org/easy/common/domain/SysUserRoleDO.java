package org.easy.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.util.Date;
import java.io.Serializable;

/**
 * (SysUserRole)实体类
 *
 * @author makejava
 * @since 2021-09-26 14:27:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@TableName(value = "sys_user_role")
public class SysUserRoleDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -14266400977799297L;
    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 角色id
     */
    private String roleId;

}
