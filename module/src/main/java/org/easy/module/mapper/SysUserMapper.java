package org.easy.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.easy.common.domain.SysUserDO;

/**
 * @ClassName: SysUserMapper
 * @Description: 用户Mapper
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserDO> {
}
