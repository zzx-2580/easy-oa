package org.easy.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.vo.SysDictDataVO;

import java.util.List;

/**
 * @ClassName: SysDictDataMapper
 * @Description: 数据字典-数据mapper
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Mapper
public interface SysDictDataMapper extends BaseMapper<SysDictDataDO> {

    // 查询
    List<SysDictDataVO> queryList();

}
