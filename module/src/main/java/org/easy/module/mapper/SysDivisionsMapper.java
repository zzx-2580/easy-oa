package org.easy.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.easy.common.domain.SysDivisionsDO;

/**
 * 行政区域表mapper
 * @Author ZhouZhiXin
 * @Date 2022/10/14 13:27
 */
@Mapper
public interface SysDivisionsMapper extends BaseMapper<SysDivisionsDO> {
}
