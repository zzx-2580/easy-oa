package org.easy.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.easy.common.domain.SysDictDO;
import org.easy.common.vo.SysDictVO;

import java.util.List;

/**
 * @ClassName: SysDictMapper
 * @Description: 数据字典mapper
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDictDO> {

    // 查询
    List<SysDictVO> queryList();

}
