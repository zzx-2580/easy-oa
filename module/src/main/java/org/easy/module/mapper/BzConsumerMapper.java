package org.easy.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.easy.common.domain.BzConsumerDO;

import java.util.List;

/**
 * @ClassName: BzConsumerMapper
 * @Description: 客户信息Mapper
 * @Author zhouzhixin
 * @Date 2021/11/26
 * @Version 1.0
 */
@Mapper
public interface BzConsumerMapper extends BaseMapper<BzConsumerDO> {

    List<BzConsumerDO> getConsumerByPage(@Param("pageNo") Long pageNo, @Param("pageSize") Long pageSize);

}
