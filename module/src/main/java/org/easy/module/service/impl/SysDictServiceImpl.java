package org.easy.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.easy.common.constant.TipConstant;
import org.easy.common.domain.SysDictDO;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.vo.SysDictVO;
import org.easy.module.mapper.SysDictDataMapper;
import org.easy.module.mapper.SysDictMapper;
import org.easy.module.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: SysDictServiceImpl
 * @Description: 数据字典service实现类
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDictDO> implements SysDictService {

    SysDictMapper sysDictMapper;

    SysDictDataMapper sysDictDataMapper;

    @Autowired
    public void init(SysDictMapper sysDictMapper, SysDictDataMapper sysDictDataMapper) {
        this.sysDictMapper = sysDictMapper;
        this.sysDictDataMapper = sysDictDataMapper;
    }

    /**
     * <p>列表查询</p>
     *
     * @param
     * @return java.util.List<org.easy.common.vo.SysDictVO>
     * @author zhouzhixin
     * @date 2021/10/13 14:11
     */
    @Override
    public List<SysDictVO> queryList() {
        List<SysDictVO> dictVOList = sysDictMapper.queryList();
        // 非空判断，为空则返回空集合
        if (CollectionUtils.isEmpty(dictVOList)) {
            dictVOList = new ArrayList<>();
        }
        return dictVOList;
    }

    /**
     * <p>删除</p>
     *
     * @param id
     * @return java.lang.Boolean
     * @author zhouzhixin
     * @date 2021/10/11 16:20
     */
    @Transactional
    @Override
    public Boolean delete(String id) {
        boolean flag = true;
        // 先删主表再删从表
        int dictDelFlag = sysDictMapper.deleteById(id);
        int dataDelFlag = sysDictDataMapper.delete(new QueryWrapper<SysDictDataDO>().lambda()
                .eq(SysDictDataDO::getDictId, id));
        if (!(dictDelFlag > 0 && dataDelFlag > 0)) {
            flag = false;
        }
        return flag;
    }

    /**
     * <p>验证是否存在</p>
     *
     * @param sysDictDO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 16:20
     */
    @Override
    public String checkDict(SysDictDO sysDictDO) {
        List<SysDictDO> dictDOList;
        dictDOList = sysDictMapper.selectList(new QueryWrapper<SysDictDO>().lambda()
                .eq(SysDictDO::getDictName, sysDictDO.getDictName()));
        if (!CollectionUtils.isEmpty(dictDOList)) {
            return TipConstant.CHECK_DICT_EXIST_DICT_NAME;
        }
        dictDOList = sysDictMapper.selectList(new QueryWrapper<SysDictDO>().lambda()
                .eq(SysDictDO::getDictCode, sysDictDO.getDictCode()));
        if (!CollectionUtils.isEmpty(dictDOList)) {
            return TipConstant.CHECK_DICT_EXIST_DICT_CODE;
        }
        return null;
    }
}
