package org.easy.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.easy.common.constant.TipConstant;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.domain.SysDivisionsDO;
import org.easy.common.vo.SysDictDataVO;
import org.easy.module.mapper.SysDictDataMapper;
import org.easy.module.mapper.SysDivisionsMapper;
import org.easy.module.service.SysDictDataService;
import org.easy.module.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @ClassName: SysDictDataServiceImpl
 * @Description: 数据字典-数据实现类
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Slf4j
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictDataDO> implements SysDictDataService {

    SysDictDataMapper sysDictDataMapper;
    SysDictService sysDictService;
    SysDivisionsMapper sysDivisionsMapper;

    @Autowired
    public void init(SysDictDataMapper sysDictDataMapper, SysDictService sysDictService, SysDivisionsMapper sysDivisionsMapper) {
        this.sysDictDataMapper = sysDictDataMapper;
        this.sysDictService = sysDictService;
        this.sysDivisionsMapper = sysDivisionsMapper;
    }

    /**
     * <p>列表查询</p>
     *
     * @param
     * @return java.util.List<org.easy.common.vo.SysDictDataVO>
     * @author zhouzhixin
     * @date 2021/10/29 14:17
     */
    @Override
    public List<SysDictDataVO> queryList() {
        List<SysDictDataVO> dictDataVOList = sysDictDataMapper.queryList();
        // 非空判断，为空则返回空集合
        if (CollectionUtils.isEmpty(dictDataVOList)) {
            dictDataVOList = new ArrayList<>();
        }
        return dictDataVOList;
    }

    /**
     * <p>验证字典数据是否存在</p>
     *
     * @param sysDictDataDO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 16:40
     */
    @Override
    public String checkData(SysDictDataDO sysDictDataDO) {
        List<SysDictDataDO> dictDataDOList;
        dictDataDOList = sysDictDataMapper.selectList(new QueryWrapper<SysDictDataDO>().lambda()
                .eq(SysDictDataDO::getDataName, sysDictDataDO.getDataName())
                .eq(SysDictDataDO::getDataValue, sysDictDataDO.getDataValue()));
        if (!CollectionUtils.isEmpty(dictDataDOList)) {
            return TipConstant.CHECK_DICT_DATA_EXIST_DATA;
        }
        return null;
    }

    // ------------------------测试方法------------------------

    @Override
    public void testTransactional(SysDictDataDO sysDictDataDO) {
        try {
            sysDictDataDO.setDataDesc("2");
            sysDictDataMapper.updateById(sysDictDataDO);
            updateData(sysDictDataDO);
            int update = 1 / 0;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public void updateData(SysDictDataDO sysDictDataDO) {
        sysDictDataDO.setId("7653492343132161");
        sysDictDataDO.setDataDesc("2");
        int update = sysDictDataMapper.updateById(sysDictDataDO);
    }

    @Override
    public void testParallelSearch1() {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 3, 1,
                TimeUnit.SECONDS, new ArrayBlockingQueue<>(3),
                Executors.defaultThreadFactory(), new ThreadPoolExecutor.DiscardPolicy());
        try {
            CompletableFuture<Long> longCompletableFuture1 = CompletableFuture.supplyAsync(() -> {
                Page<SysDivisionsDO> page1 = new Page<>(1, 10000);
                IPage<SysDivisionsDO> result1 = sysDivisionsMapper.selectPage(page1, new QueryWrapper<>());
                log.info(Thread.currentThread().getName() + " 完成");
                return result1.getSize();
            }, executor);
            CompletableFuture<Long> longCompletableFuture2 = CompletableFuture.supplyAsync(() -> {
                Page<SysDivisionsDO> page2 = new Page<>(1, 10000);
                IPage<SysDivisionsDO> result2 = sysDivisionsMapper.selectPage(page2, new QueryWrapper<>());
                log.info(Thread.currentThread().getName() + " 完成");
                return result2.getSize();
            }, executor);
            CompletableFuture<Long> longCompletableFuture3 = CompletableFuture.supplyAsync(() -> {
                Page<SysDivisionsDO> page3 = new Page<>(1, 10000);
                IPage<SysDivisionsDO> result3 = sysDivisionsMapper.selectPage(page3, new QueryWrapper<>());
                log.info(Thread.currentThread().getName() + " 完成");
                return result3.getSize();
            }, executor);
            CompletableFuture.allOf(longCompletableFuture1, longCompletableFuture2, longCompletableFuture3).join();
            log.info(longCompletableFuture1.get() + longCompletableFuture2.get() + longCompletableFuture3.get() +
                    Thread.currentThread().getName());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    @Override
    public void testParallelSearch2() {
        Page<SysDivisionsDO> page1 = new Page<>(1, 10000);
        IPage<SysDivisionsDO> result1 = sysDivisionsMapper.selectPage(page1, new QueryWrapper<>());
        Page<SysDivisionsDO> page2 = new Page<>(1, 10000);
        IPage<SysDivisionsDO> result2 = sysDivisionsMapper.selectPage(page2, new QueryWrapper<>());
        Page<SysDivisionsDO> page3 = new Page<>(1, 10000);
        IPage<SysDivisionsDO> result3 = sysDivisionsMapper.selectPage(page3, new QueryWrapper<>());
        log.info(String.valueOf(result1.getSize() + result2.getSize() + result3.getSize()));
    }
}
