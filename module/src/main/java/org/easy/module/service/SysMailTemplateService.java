package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.domain.SysMailTemplateDO;
import org.easy.common.vo.SysMailTemplateVO;

import java.util.List;

/**
 * @ClassName: SysUserServiceImpl
 * @Description: 用户Service
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
public interface SysMailTemplateService extends IService<SysMailTemplateDO> {

    // 查询
    List<SysMailTemplateVO> queryList();

    // 验证是否存在
    String checkData(SysMailTemplateDO sysMailTemplateDO);

}
