package org.easy.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.easy.common.constant.TipConstant;
import org.easy.common.domain.SysMailTemplateDO;
import org.easy.common.vo.SysMailTemplateVO;
import org.easy.module.mapper.SysMailTemplateMapper;
import org.easy.module.service.SysMailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: SysUserServiceImpl
 * @Description: 用户Service实现类
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
@Slf4j
@Service
public class SysMailTemplateServiceImpl extends ServiceImpl<SysMailTemplateMapper, SysMailTemplateDO> implements SysMailTemplateService {

    SysMailTemplateMapper sysMailTemplateMapper;

    @Autowired
    public void init(SysMailTemplateMapper sysMailTemplateMapper) {
        this.sysMailTemplateMapper = sysMailTemplateMapper;
    }

    /**
     * <p>列表查询</p>
     *
     * @param
     * @return java.util.List<org.easy.common.vo.SysMailTemplateVO>
     * @author zhouzhixin
     * @date 2021/10/29 14:18
     */
    @Override
    public List<SysMailTemplateVO> queryList() {
        List<SysMailTemplateVO> mailTemplateVOList = sysMailTemplateMapper.queryList();
        // 非空判断，为空则返回空集合
        if (CollectionUtils.isEmpty(mailTemplateVOList)) {
            mailTemplateVOList = new ArrayList<>();
        }
        return mailTemplateVOList;
    }

    /**
     * <p>验证邮件模板数据是否存在</p>
     *
     * @param sysMailTemplateDO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 14:18
     */
    @Override
    public String checkData(SysMailTemplateDO sysMailTemplateDO) {
        List<SysMailTemplateDO> mailTemplateDOList;
        mailTemplateDOList = sysMailTemplateMapper.selectList(new QueryWrapper<SysMailTemplateDO>().lambda()
                .eq(SysMailTemplateDO::getSubject, sysMailTemplateDO.getSubject()));
        if (!CollectionUtils.isEmpty(mailTemplateDOList)) {
            return TipConstant.CHECK_MAIL_TEMPLATE_EXIST_SUBJECT;
        }
        return null;
    }
}
