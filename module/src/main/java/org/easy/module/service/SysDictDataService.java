package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.vo.SysDictDataVO;

import java.util.List;

/**
 * @ClassName: SysDictDataService
 * @Description: 数据字典-数据Service
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
public interface SysDictDataService extends IService<SysDictDataDO> {

    // 查询
    List<SysDictDataVO> queryList();

    // 验证是否存在
    String checkData(SysDictDataDO sysDictDataDO);

    // 事务测试
    void testTransactional(SysDictDataDO sysDictDataDO);

    void updateData(SysDictDataDO sysDictDataDO);

    void testParallelSearch1();

    void testParallelSearch2();

}
