package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.SysUserDO;

/**
 * @ClassName: SysUserServiceImpl
 * @Description: 用户Service
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
public interface SysUserService extends IService<SysUserDO> {

    // 添加用户
    String register(SysUserDO sysUserDO);

    // 验证用户信息
    String checkUserInfo(SysUserDO sysUserDO);

    // 激活用户
    String activation(SysUserDO sysUserDO, String checkCode) throws InterruptedException;

    // 发送激活邮件
    void sendActivationEmail(SysUserDO sysUserDO);

}
