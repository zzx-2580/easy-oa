package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.SysDictDO;
import org.easy.common.vo.SysDictVO;

import java.util.List;

/**
 * @ClassName: SysDictService
 * @Description: 数据字典service
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
public interface SysDictService extends IService<SysDictDO> {

    // 查询
    List<SysDictVO> queryList();

    // 删除
    Boolean delete(String id);

    // 验证是否存在
    String checkDict(SysDictDO sysDictDO);

}
