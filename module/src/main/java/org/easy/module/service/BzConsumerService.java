package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.BzConsumerDO;

import java.util.List;

/**
 * @ClassName: BzConsumerService
 * @Description: 客户信息Service
 * @Author zhouzhixin
 * @Date 2021/11/26
 * @Version 1.0
 */
public interface BzConsumerService extends IService<BzConsumerDO> {

    List<BzConsumerDO> getAllConsumerTest() throws InterruptedException;

}
