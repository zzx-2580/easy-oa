package org.easy.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.easy.common.domain.SysMailRecordDO;
import org.easy.common.domain.SysMailTemplateDO;
import org.easy.module.mapper.SysMailRecordMapper;
import org.easy.module.mapper.SysMailTemplateMapper;
import org.easy.module.service.SysMailRecordService;
import org.easy.module.service.SysMailTemplateService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SysUserServiceImpl
 * @Description: 用户Service实现类
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
@Slf4j
@Service
public class SysMailRecordServiceImpl extends ServiceImpl<SysMailRecordMapper, SysMailRecordDO> implements SysMailRecordService {
}
