package org.easy.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.easy.common.domain.SysMailRecordDO;

/**
 * @ClassName: SysUserServiceImpl
 * @Description: 用户Service
 * @Author zhouzhixin
 * @Date 2021/9/26
 * @Version 1.0
 */
public interface SysMailRecordService extends IService<SysMailRecordDO> {
}
