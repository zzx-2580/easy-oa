package org.easy.module;

import org.easy.common.util.EncryptUtil;
import org.junit.jupiter.api.Test;

import java.util.UUID;

/**
 * @ClassName: EncryptTests
 * @Description: 加密工具类测试
 * @Author zhouzhixin
 * @Date 2021/9/28
 * @Version 1.0
 */
public class EncryptTests {

    @Test
    public void test1() {
        String salt = "b6ce77c3-9ac3-4bf4-9c7a-5d06aa6f9882";
        System.out.println(salt);
        String encrypt = EncryptUtil.encrypt("zzx6255873", salt, "SHA-256", 1000);
        System.out.println(encrypt);
    }

}
