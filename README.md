# Easy-OA

#### 介绍
一个简单的OA系统

#### 软件架构
springBoot + shiro + mybatis-plus + fastjson + lombok + thymeleaf

#### 安装教程
1.  安装JDK1.8
2.  安装maven对root模块进行install&package操作获取control模块target文件夹下的jar包
3.  双击jar包后台运行（此时80端口会被占用，也可以cmd操作，具体操作方式自行查找）
4.  关闭时cmd输入netstat -ano获取80端口对应的pid
5.  cmd输入taskkill /f /t /pid <端口号> 回车即可

#### 说明
该项目是在idea jdk1.8 单节点mysql redis的环境下开发的。

项目初期开发，正在逐步完善，后续会添加到es、swagger和Alibaba sentinel。

项目数据库结构在项目根目录下的other文件夹里。

项目准备添加内容：数据字典、邮件模板

作者微信：Virgo_zhixin，反馈意见可以给到我微信上。