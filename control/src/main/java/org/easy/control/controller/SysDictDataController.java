package org.easy.control.controller;

import lombok.extern.slf4j.Slf4j;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.vo.SysDictDataVO;
import org.easy.module.service.SysDictDataService;
import org.easy.module.service.SysDictService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: SysDictDataController
 * @Description: 数据字典-数据Controller
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Slf4j
@RequestMapping("/dict/data")
@Controller
public class SysDictDataController {

    SysDictService sysDictService;

    SysDictDataService sysDictDataService;

    @Autowired
    public void init(SysDictService sysDictService, SysDictDataService sysDictDataService) {
        this.sysDictService = sysDictService;
        this.sysDictDataService = sysDictDataService;
    }

    /**
     * <p>列表查询</p>
     *
     * @param model
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 11:10
     */
    @GetMapping("/query")
    public String query(Model model) {
        log.info("数据字典-数据列表查询---start");
        try {
            model.addAttribute("dictDataVOList", sysDictDataService.queryList());
            model.addAttribute("dictVOList", sysDictService.queryList());
        } catch (Exception e) {
            log.error("数据字典数据查询过程出现异常", e);
        } finally {
            log.info("数据字典-数据列表查询---end");
        }
        return "system/dictionary-data";
    }

    /**
     * <p>新增</p>
     *
     * @param sysDictDataVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 11:10
     */
    @PostMapping("/add")
    public String add(SysDictDataVO sysDictDataVO) {
        log.info("数据字典-数据新增---start");
        try {
            SysDictDataDO sysDictDataDO = new SysDictDataDO();
            BeanUtils.copyProperties(sysDictDataVO, sysDictDataDO);
            sysDictDataService.save(sysDictDataDO);
        } catch (Exception e) {
            log.error("数据字典数据新增过程出现异常", e);
        } finally {
            log.info("数据字典-数据新增---end");
        }
        return "redirect:/dict/data/query";
    }

    /**
     * <p>删除</p>
     *
     * @param id
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 13:47
     */
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        log.info("数据字典-数据删除---start");
        try {
            sysDictDataService.removeById(id);
        } catch (Exception e) {
            log.error("数据字典数据删除过程出现异常", e);
        } finally {
            log.info("数据字典-数据删除---start");
        }
        return "redirect:/dict/data/query";
    }

    /**
     * <p>修改</p>
     *
     * @param sysDictDataVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 14:10
     */
    @PostMapping("/update")
    public String update(SysDictDataVO sysDictDataVO) {
        log.info("数据字典-数据修改---start");
        try {
            SysDictDataDO sysDictDataDO = new SysDictDataDO();
            BeanUtils.copyProperties(sysDictDataVO, sysDictDataDO);
            sysDictDataService.updateById(sysDictDataDO);
        } catch (Exception e) {
            log.error("数据字典数据修改过程出现异常", e);
        } finally {
            log.info("数据字典-数据修改---start");
        }
        return "redirect:/dict/data/query";
    }

    /**
     * <p>验证字典数据是否存在</p>
     *
     * @param sysDictDataVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 16:40
     */
    @ResponseBody
    @PostMapping("checkData")
    public String checkData(SysDictDataVO sysDictDataVO) {
        log.info("验证字典数据是否存在---start");
        try {
            SysDictDataDO sysDictDataDO = new SysDictDataDO();
            BeanUtils.copyProperties(sysDictDataVO, sysDictDataDO);
            return sysDictDataService.checkData(sysDictDataDO);
        } catch (Exception e) {
            log.error("验证字典数据是否存在出现异常", e);
        } finally {
            log.info("验证字典数据是否存在---end");
        }
        return null;
    }

}
