package org.easy.control.controller;

import lombok.extern.slf4j.Slf4j;
import org.easy.common.domain.SysDictDataDO;
import org.easy.common.domain.SysMailTemplateDO;
import org.easy.common.vo.SysDictDataVO;
import org.easy.common.vo.SysMailTemplateVO;
import org.easy.module.service.SysMailTemplateService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: SysMailTemplateController
 * @Description: 邮件模板Controller
 * @Author zhouzhixin
 * @Date 2021/10/29
 * @Version 1.0
 */
@Slf4j
@RequestMapping("/mail/template")
@Controller
public class SysMailTemplateController {

    SysMailTemplateService sysMailTemplateService;

    @Autowired
    public void init(SysMailTemplateService sysMailTemplateService) {
        this.sysMailTemplateService = sysMailTemplateService;
    }

    /**
     * <p>列表查询</p>
     *
     * @param model
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 13:46
     */
    @GetMapping("/query")
    public String query(Model model) {
        log.info("邮件模板列表查询---start");
        try {
            model.addAttribute("mailTemplateVOList", sysMailTemplateService.queryList());
        } catch (Exception e) {
            log.error("邮件模板列表查询过程出现异常", e);
        } finally {
            log.info("邮件模板列表查询---end");
        }
        return "system/mailTemplate";
    }

    /**
     * <p>新增</p>
     *
     * @param sysMailTemplateVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 14:02
     */
    @PostMapping("/add")
    public String add(SysMailTemplateVO sysMailTemplateVO) {
        log.info("邮件模板新增---start");
        try {
            SysMailTemplateDO sysMailTemplateDO = new SysMailTemplateDO();
            BeanUtils.copyProperties(sysMailTemplateVO, sysMailTemplateDO);
            sysMailTemplateService.save(sysMailTemplateDO);
        } catch (Exception e) {
            log.error("邮件模板新增过程出现异常", e);
        } finally {
            log.info("邮件模板新增---end");
        }
        return "redirect:/mail/template/query";
    }

    /**
     * <p>删除</p>
     *
     * @param id
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 14:02
     */
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        log.info("邮件模板删除---start");
        try {
            sysMailTemplateService.removeById(id);
        } catch (Exception e) {
            log.error("邮件模板删除过程出现异常", e);
        } finally {
            log.info("邮件模板删除---start");
        }
        return "redirect:/mail/template/query";
    }

    /**
     * <p>修改</p>
     *
     * @param sysMailTemplateVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 14:07
     */
    @PostMapping("/update")
    public String update(SysMailTemplateVO sysMailTemplateVO) {
        log.info("邮件模板修改---start");
        try {
            SysMailTemplateDO sysMailTemplateDO = new SysMailTemplateDO();
            BeanUtils.copyProperties(sysMailTemplateVO, sysMailTemplateDO);
            sysMailTemplateService.save(sysMailTemplateDO);
        } catch (Exception e) {
            log.error("邮件模板修改过程出现异常", e);
        } finally {
            log.info("邮件模板修改---end");
        }
        return "redirect:/mail/template/query";
    }

    /**
     * <p>验证邮件模板数据是否存在</p>
     * @param sysMailTemplateVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/29 14:21
     */
    @ResponseBody
    @PostMapping("checkData")
    public String checkData(SysMailTemplateVO sysMailTemplateVO) {
        log.info("验证邮件模板数据是否存在---start");
        try {
            SysMailTemplateDO sysMailTemplateDO = new SysMailTemplateDO();
            BeanUtils.copyProperties(sysMailTemplateVO, sysMailTemplateDO);
            sysMailTemplateService.checkData(sysMailTemplateDO);
        } catch (Exception e) {
            log.error("验证邮件模板数据是否存在出现异常", e);
        } finally {
            log.info("验证邮件模板数据是否存在---end");
        }
        return null;
    }

}
