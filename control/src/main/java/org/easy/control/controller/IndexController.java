package org.easy.control.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName: IndexController
 * @Description: 页面跳转Controller
 * @Author zhouzhixin
 * @Date 2021/9/24
 * @Version 1.0
 */
@Controller
public class IndexController {

    /**
     * 登录页
     *
     * @return
     */
    @GetMapping("sign-in")
    public String signIn() {
        return "sign-in";
    }

    /**
     * 注册页
     *
     * @returnsign-up
     */
    @GetMapping("sign-up")
    public String signUp() {
        return "sign-up";
    }

    /**
     * 邮箱验证页
     *
     * @return
     */
    @GetMapping("email-check")
    public String emailCheck() {
        return "email-check";
    }

    /**
     * 首页
     *
     * @return
     */
    @GetMapping("/index")
    public String index() {
        return "index";
    }

}
