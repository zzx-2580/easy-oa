package org.easy.control.controller;

import lombok.extern.slf4j.Slf4j;
import org.easy.common.domain.SysDictDO;
import org.easy.common.vo.SysDictVO;
import org.easy.module.service.SysDictDataService;
import org.easy.module.service.SysDictService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: DictController
 * @Description: 数据字典Controller
 * @Author zhouzhixin
 * @Date 2021/10/9
 * @Version 1.0
 */
@Slf4j
@RequestMapping("/dict")
@Controller
public class SysDictController {

    SysDictService sysDictService;

    SysDictDataService sysDictDataService;

    @Autowired
    public void init(SysDictService sysDictService, SysDictDataService sysDictDataService) {
        this.sysDictService = sysDictService;
        this.sysDictDataService = sysDictDataService;
    }

    /**
     * <p>列表查询</p>
     *
     * @param model
     * @return java.lang.String
     * @author zhouzhixin8
     * @date 2021/10/9 11:11
     */
    @GetMapping("/query")
    public String query(Model model) {
        log.info("数据字典列表查询---start");
        try {
            model.addAttribute("dictVOList", sysDictService.queryList());
        } catch (Exception e) {
            log.error("数据字典列表查询过程出现异常", e);
        } finally {
            log.info("数据字典列表查询---end");
        }
        return "system/dictionary";
    }

    /**
     * <p>新增</p>
     *
     * @param sysDictVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/9 13:33
     */
    @PostMapping("/add")
    public String add(SysDictVO sysDictVO) {
        log.info("数据字典新增---start");
        try {
            SysDictDO sysDictDO = new SysDictDO();
            BeanUtils.copyProperties(sysDictVO, sysDictDO);
            sysDictService.save(sysDictDO);
        } catch (Exception e) {
            log.error("数据字典新增过程出现异常", e);
        } finally {
            log.info("数据字典新增---end");
        }
        return "redirect:/dict/query";
    }

    /**
     * <p>修改</p>
     *
     * @param sysDictVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/11 10:15
     */
    @PostMapping("/update")
    public String update(SysDictVO sysDictVO) {
        log.info("数据字典修改---start");
        try {
            SysDictDO sysDictDO = new SysDictDO();
            BeanUtils.copyProperties(sysDictVO, sysDictDO);
            sysDictService.updateById(sysDictDO);
        } catch (Exception e) {
            log.error("数据字典修改过程出现异常", e);
        } finally {
            log.info("数据字典修改---end");
        }
        return "redirect:/dict/query";
    }

    /**
     * <p>删除</p>
     *
     * @param id
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/9 14:21
     */
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        log.info("数据字典删除---start");
        try {
            Boolean flag = sysDictService.delete(id);
            log.info("数据字典删除返回结果:{}", flag);
        } catch (Exception e) {
            log.error("数据字典删除过程出现异常", e);
        } finally {
            log.info("数据字典删除---end");
        }
        return "redirect:/dict/query";
    }

    /**
     * <p>验证是否存在</p>
     *
     * @param sysDictVO
     * @return java.lang.String
     * @author zhouzhixin
     * @date 2021/10/9 16:25
     */
    @ResponseBody
    @PostMapping("checkDict")
    public String checkDict(SysDictVO sysDictVO) {
        log.info("验证字典是否存在---start");
        try {
            SysDictDO sysDictDO = new SysDictDO();
            BeanUtils.copyProperties(sysDictVO, sysDictDO);
            return sysDictService.checkDict(sysDictDO);
        } catch (Exception e) {
            log.error("验证字典是否存在出现异常", e);
        } finally {
            log.info("验证字典是否存在---end");
        }
        return null;
    }

}
