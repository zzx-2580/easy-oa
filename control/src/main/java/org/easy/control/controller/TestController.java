package org.easy.control.controller;

import lombok.extern.slf4j.Slf4j;
import org.easy.common.domain.SysDictDataDO;
import org.easy.module.service.SysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 周志鑫
 */
@Slf4j
@RequestMapping("/test")
@RestController
public class TestController {

    @Autowired
    SysDictDataService sysDictDataService;

    @PostMapping("/testTransactional")
    public void testTransactional(@RequestBody SysDictDataDO sysDictDataDO) {
        try {
            sysDictDataService.testTransactional(sysDictDataDO);
            sysDictDataService.updateData(sysDictDataDO);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("catch");
        }
    }

    @GetMapping("/testParallelSearch")
    public void testParallelSearch() {
        sysDictDataService.testParallelSearch1();
    }

}
