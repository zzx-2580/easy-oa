package org.easy.control.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: SysTestController
 * @Description: 测试所用controller
 * @Author zhouzhixin
 * @Date 2021/10/12
 * @Version 1.0
 */
@Slf4j
@RequestMapping("/test")
@RestController
public class SysTestController {

}
