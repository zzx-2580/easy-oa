package org.easy.control.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName: WebConfig
 * @Description: MVC配置类
 * @Author zhouzhixin
 * @Date 2021/9/24
 * @Version 1.0
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    /**
     * <p>视图解析器（配合thymeleaf使用）</p>
     *
     * @param registry
     * @return void
     * @author zhouzhixin
     * @date 2021/9/24 10:56
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("countdown");
    }

    /**
     * <p>静态资源文件映射</p>
     * @param registry
     * @return void
     * @author zhouzhixin
     * @date 2021/9/24 13:55
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/templates/")
                .addResourceLocations("classpath:/static/");
    }

}
