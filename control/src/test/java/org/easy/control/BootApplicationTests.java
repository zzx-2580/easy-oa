package org.easy.control;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootApplicationTests {

    @Autowired
    private DataSource dataSource;

    @Resource
    private TemplateEngine templateEngine;

    @Test
    public void test1() {
        int nextInt = RandomUtils.nextInt(100000, 999999);
        System.out.println(nextInt);
    }

    @Test
    public void test2() {
        //看一下默认数据源
        System.out.println(dataSource.getClass());
        //获得连接
        try (Connection connection = dataSource.getConnection()) {
            System.out.println(connection);
            DruidDataSource druidDataSource = (DruidDataSource) dataSource;
            System.out.println("druidDataSource 数据源最大连接数：" + druidDataSource.getMaxActive());
            System.out.println("druidDataSource 数据源初始化连接数：" + druidDataSource.getInitialSize());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
